package com.kolsuma.gitlab.notification;

import static java.util.stream.Collectors.toList;

import com.kolsuma.gitlab.notification.data.Compare;
import com.kolsuma.gitlab.notification.data.Tag;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class ReleaseNotifier {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private RestTemplate restTemplate = new RestTemplate();

    private GitLabProperties gitlab;
    private Mailer mailer;

    public ReleaseNotifier(GitLabProperties gitlab, Mailer mailer) {
        this.gitlab = gitlab;
        this.mailer = mailer;
    }

    public void emailReleaseNotification(String projectPath, String tagRef) {
        String tagName = tagRef.replaceAll(".*\\/(.*)", "$1");
        String projectName = projectPath.replaceAll(".*\\/(.*)", "$1");

        log.info("Attempting to send email notification for {} {}", projectPath, tagName);

        String[] commitIds = getCommitIdsToAndFrom(projectPath, tagName);

        if (commitIds == null) {
            log.error("Unable to find release tag {} in project {}", tagRef, projectPath);
            return;
        }

        String toCommitId   = commitIds[0];
        String fromCommitId = commitIds[1];

        List<String> commitTitles = getCommitTitles(projectPath, fromCommitId, toCommitId);

        if (commitTitles.isEmpty()) {
            log.error("Unable to find any commits from {} to {}", fromCommitId, toCommitId);
            return;
        }

        try {
            mailer.sendEmailNotification(projectName, tagName, commitTitles);
        } catch (IOException | TemplateException | MessagingException e) {
            log.error("Unexpected error encountered trying to send email", e);
        }
    }

    private String[] getCommitIdsToAndFrom(String projectPath, String tagName) {
        Tag[] tags = restTemplate.getForObject(
                gitlab.url() + "/api/v4/projects/{project}/repository/tags?private_token={private_token}",
                Tag[].class, projectPath, gitlab.personalAccessToken());

        for (int i=0; i<tags.length-1; i++) {
            if (tags[i].name.equals(tagName)) {
                Tag to   = tags[i];
                Tag from = tags[i+1];
                return new String[]{to.commit.id, from.commit.id};
            }
        }

        return null;
    }

    private List<String> getCommitTitles(String projectPath, String fromCommitId, String toCommitId) {
        Compare compare = restTemplate.getForObject(
                gitlab.url() + "/api/v4/projects/{project}/repository/compare?" +
                        "from={from}&to={to}&private_token={private_token}",
                Compare.class, projectPath, fromCommitId, toCommitId, gitlab.personalAccessToken());

        return Arrays.stream(compare.commits)
              .map(commit -> commit.title)
              .collect(toList());
    }
}
