package com.kolsuma.gitlab.notification;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.ResponseEntity.status;

import com.kolsuma.gitlab.notification.data.Project;
import com.kolsuma.gitlab.notification.data.WebhookEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebhookController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private GitLabProperties gitLab;
    private ReleaseNotifier releaseNotifier;

    public WebhookController(GitLabProperties gitLab, ReleaseNotifier releaseNotifier) {
        this.gitLab = gitLab;
        this.releaseNotifier = releaseNotifier;
    }

    @PostMapping(value = "/event")
    public ResponseEntity<String> index(@RequestBody WebhookEvent event,
                                        @RequestHeader(value="X-Gitlab-Event") String eventTypeHeader,
                                        @RequestHeader(value="X-Gitlab-Token") String secretTokenHeader) {

        if (eventTypeHeader == null || !eventTypeHeader.equals("Tag Push Hook")) {
            log.debug("Ignoring event {}", eventTypeHeader);
            return status(OK).body("");
        }

        if (!gitLab.secretToken().equals(secretTokenHeader)) {
            return status(UNAUTHORIZED).body("");
        }

        if (missingRequiredDataIn(event)) {
            return status(OK).body("");
        }

        Project project = event.project;
        releaseNotifier.emailReleaseNotification(project.path_with_namespace, event.ref);

        return status(OK).body("");
    }

    private boolean missingRequiredDataIn(WebhookEvent event) {
        boolean missingData = false;

        if (event.ref == null) {
            log.error("Missing expected data event.ref");
            missingData = true;
        }

        if (event.project == null || event.project.path_with_namespace == null) {
            log.error("Missing expected data event.project.path_with_namespace");
            missingData = true;
        }

        return missingData;
    }
}
