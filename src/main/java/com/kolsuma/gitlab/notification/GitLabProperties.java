package com.kolsuma.gitlab.notification;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Component
@Validated
@ConfigurationProperties("gitlab")
public class GitLabProperties {

    @NotBlank
    @Pattern(regexp = "https?\\://[a-z0-9.-:]+")
    private String url;

    @NotBlank
    @Pattern(regexp = "[a-zA-Z0-9]+")
    private String secretToken;

    @NotBlank
    @Pattern(regexp = "[a-zA-Z0-9]+")
    private String personalAccessToken;

    public String url() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String secretToken() {
        return secretToken;
    }

    public void setSecretToken(String secretToken) {
        this.secretToken = secretToken;
    }

    public String personalAccessToken() {
        return personalAccessToken;
    }

    public void setPersonalAccessToken(String personalAccessToken) {
        this.personalAccessToken = personalAccessToken;
    }
}
