package com.kolsuma.gitlab.notification;

import static org.springframework.ui.freemarker.FreeMarkerTemplateUtils.processTemplateIntoString;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Mailer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private EmailProperties email;
    private JavaMailSender mailSender;
    private Template emailTemplate;

    public Mailer(EmailProperties emailProperties, JavaMailSender mailSender, Configuration freemarkerConfiguration)
            throws Exception {
        this.email = emailProperties;
        this.mailSender = mailSender;
        this.emailTemplate = freemarkerConfiguration.getTemplate("release-email.ftl");
    }

    public void sendEmailNotification(String project, String release, List<String> commitTitles)
            throws IOException, TemplateException, MessagingException {

        Map<String, Object> data = new HashMap<>();
        data.put("project", project);
        data.put("release", release);
        data.put("commitTitles", commitTitles);

        String html = processTemplateIntoString(emailTemplate, data);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setSubject(project + " " + release);
        helper.setTo(email.addressTo());
        helper.setFrom(email.addressFrom(), email.personalFrom());
        helper.setText(html, true);

        mailSender.send(message);

        log.info("Email notification '{} {}' sent to {}", project, release, email.addressTo());
    }
}
