package com.kolsuma.gitlab.notification.data;

public class Commit {

    public String id;
    public String title;

    @Override
    public String toString() {
        return "Commit{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
