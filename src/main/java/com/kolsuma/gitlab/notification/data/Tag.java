package com.kolsuma.gitlab.notification.data;

public class Tag {

    public String name;
    public Commit commit;

    @Override
    public String toString() {
        return "Tag{" +
                "name='" + name + '\'' +
                ", commit=" + commit +
                '}';
    }
}
