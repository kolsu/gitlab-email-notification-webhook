package com.kolsuma.gitlab.notification.data;

public class Project {

    public String path_with_namespace;

    @Override
    public String toString() {
        return "Project{" +
                "path_with_namespace='" + path_with_namespace + '\'' +
                '}';
    }
}
