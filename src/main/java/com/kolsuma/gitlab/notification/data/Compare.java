package com.kolsuma.gitlab.notification.data;

import java.util.Arrays;

public class Compare {

    public Commit[] commits;

    @Override
    public String toString() {
        return "Compare{" +
                "commits=" + Arrays.toString(commits) +
                '}';
    }
}
