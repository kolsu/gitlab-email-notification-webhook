package com.kolsuma.gitlab.notification.data;

public class WebhookEvent {

    public String ref;
    public Project project = new Project();

    @Override
    public String toString() {
        return "WebhookEvent{" +
                "ref='" + ref + '\'' +
                ", project=" + project +
                '}';
    }
}
