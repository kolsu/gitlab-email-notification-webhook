package com.kolsuma.gitlab.notification.integration;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.kolsuma.gitlab.notification.GitLabProperties;
import com.kolsuma.gitlab.notification.ReleaseNotifier;
import com.kolsuma.gitlab.notification.WebhookController;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(WebhookController.class)
@EnableConfigurationProperties(GitLabProperties.class)
@TestPropertySource(properties = "gitlab.secret-token = TestSecretToken")
@Tag("integration")
class WebhookControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReleaseNotifier releaseNotifier;

    @Test
    void pushTagEvent() throws Exception {
        mvc.perform(post("/event")
                .header("X-Gitlab-Event", "Tag Push Hook")
                .header("X-Gitlab-Token", "TestSecretToken")
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"object_kind\": \"tag_push\",\n" +
                        "  \"before\": \"0000000000000000000000000000000000000000\",\n" +
                        "  \"after\": \"82b3d5ae55f7080f1e6022629cdb57bfae7cccc7\",\n" +
                        "  \"ref\": \"refs/tags/v1.0.0\",\n" +
                        "  \"checkout_sha\": \"82b3d5ae55f7080f1e6022629cdb57bfae7cccc7\",\n" +
                        "  \"user_id\": 1,\n" +
                        "  \"user_name\": \"John Smith\",\n" +
                        "  \"user_avatar\": \"https://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?" +
                        "s=8://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=80\",\n" +
                        "  \"project_id\": 1,\n" +
                        "  \"project\":{\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\":\"Example\",\n" +
                        "    \"description\":\"\",\n" +
                        "    \"web_url\":\"http://example.com/jsmith/example\",\n" +
                        "    \"avatar_url\":null,\n" +
                        "    \"git_ssh_url\":\"git@example.com:jsmith/example.git\",\n" +
                        "    \"git_http_url\":\"http://example.com/jsmith/example.git\",\n" +
                        "    \"namespace\":\"Jsmith\",\n" +
                        "    \"visibility_level\":0,\n" +
                        "    \"path_with_namespace\":\"jsmith/example\",\n" +
                        "    \"default_branch\":\"master\",\n" +
                        "    \"homepage\":\"http://example.com/jsmith/example\",\n" +
                        "    \"url\":\"git@example.com:jsmith/example.git\",\n" +
                        "    \"ssh_url\":\"git@example.com:jsmith/example.git\",\n" +
                        "    \"http_url\":\"http://example.com/jsmith/example.git\"\n" +
                        "  },\n" +
                        "  \"repository\":{\n" +
                        "    \"name\": \"Example\",\n" +
                        "    \"url\": \"ssh://git@example.com/jsmith/example.git\",\n" +
                        "    \"description\": \"\",\n" +
                        "    \"homepage\": \"http://example.com/jsmith/example\",\n" +
                        "    \"git_http_url\":\"http://example.com/jsmith/example.git\",\n" +
                        "    \"git_ssh_url\":\"git@example.com:jsmith/example.git\",\n" +
                        "    \"visibility_level\":0\n" +
                        "  },\n" +
                        "  \"commits\": [],\n" +
                        "  \"total_commits_count\": 0\n" +
                        "}"))
           .andExpect(status().isOk())
        ;
    }
}