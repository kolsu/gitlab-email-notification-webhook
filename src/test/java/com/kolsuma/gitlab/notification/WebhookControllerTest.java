package com.kolsuma.gitlab.notification;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.kolsuma.gitlab.notification.data.WebhookEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class WebhookControllerTest {

    private final static String SECRET_TOKEN = "Secret-Test-Token";
    private static final String TAG_PUSH_HOOK = "Tag Push Hook";

    private WebhookController webhookController;

    private boolean emailReleaseNotificationCalled;
    private String emailReleaseNotificationCalledWithProjectPath;
    private String emailReleaseNotificationCalledWithTagRef;

    WebhookControllerTest() {
        GitLabProperties gitLabProperties = new GitLabProperties();
        gitLabProperties.setSecretToken(SECRET_TOKEN);

        ReleaseNotifierStub releaseNotifierStub = new ReleaseNotifierStub((projectPath, tagRef) -> {
            emailReleaseNotificationCalled = true;
            emailReleaseNotificationCalledWithProjectPath = projectPath;
            emailReleaseNotificationCalledWithTagRef = tagRef;
        });
        webhookController = new WebhookController(gitLabProperties, releaseNotifierStub);
    }

    @BeforeEach
    void clear() {
        emailReleaseNotificationCalled = false;
        emailReleaseNotificationCalledWithProjectPath = null;
        emailReleaseNotificationCalledWithTagRef = null;
    }

    @Test
    void invalidSecretToken() {
        ResponseEntity<String> response = webhookController.index(event(), TAG_PUSH_HOOK, "Invalid Secret Token");
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(UNAUTHORIZED)),
                () -> assertThat(emailReleaseNotificationCalled, is(false))
        );
    }

    @Test
    void nullSecretToken() {
        ResponseEntity<String> response = webhookController.index(event(), "Tag Push Hook", null);
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(UNAUTHORIZED)),
                () -> assertThat(emailReleaseNotificationCalled, is(false))
        );
    }

    @Test
    void unsupportedEventType() {
        ResponseEntity<String> response = webhookController.index(event(), "Push Hook", SECRET_TOKEN);
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(OK)),
                () -> assertThat(emailReleaseNotificationCalled, is(false))
        );
    }

    @Test
    void nullEventType() {
        ResponseEntity<String> response = webhookController.index(event(), null, SECRET_TOKEN);
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(OK)),
                () -> assertThat(emailReleaseNotificationCalled, is(false))
        );
    }

    @Test
    void emptyBody() {
        WebhookEvent emptyBody = new WebhookEvent();

        ResponseEntity<String> response = webhookController.index(emptyBody, TAG_PUSH_HOOK, SECRET_TOKEN);
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(OK)),
                () -> assertThat(emailReleaseNotificationCalled, is(false))
        );
    }

    @Test
    void validRequest() {
        ResponseEntity<String> response = webhookController.index(event(), TAG_PUSH_HOOK, SECRET_TOKEN);
        HttpStatus status = response.getStatusCode();

        assertAll(
                () -> assertThat(status, equalTo(OK)),
                () -> assertThat(emailReleaseNotificationCalled, is(true)),
                () -> assertThat(emailReleaseNotificationCalledWithProjectPath, equalTo("Dev/Project-X")),
                () -> assertThat(emailReleaseNotificationCalledWithTagRef, equalTo("Release-12"))
        );
    }

    private WebhookEvent event() {
        WebhookEvent event = new WebhookEvent();
        event.ref = "Release-12";
        event.project.path_with_namespace = "Dev/Project-X";

        return event;
    }
}
