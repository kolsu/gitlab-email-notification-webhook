package com.kolsuma.gitlab.notification;

import java.util.function.BiConsumer;

public class ReleaseNotifierStub extends ReleaseNotifier {

    private BiConsumer<String, String> consumer;

    ReleaseNotifierStub(BiConsumer<String, String> consumer) {
        super(null, null);
        this.consumer = consumer;
    }

    @Override
    public void emailReleaseNotification(String projectPath, String tagRef) {
        consumer.accept(projectPath, tagRef);
    }
}
